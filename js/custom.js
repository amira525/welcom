$(function () {
    'use strict';
    //counter jquery
    $(".counter").countUp({
        delay: 10,
        time: 1000
    });
    //
    $('body').css('paddingTop', $('.navbar').innerHeight());
    //
    $('.navbar li a.click-link, .footer .contact a.links').click(function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $('#' + $(this).data('scroll')).offset().top + 1
        }, 1000);
    });
    //
    $('.navbar ul li a').click(function () {
        $(this).addClass('active').parent().siblings().find('a').removeClass('active');
    });
    //loading screen
    $(window).load(function () {
        $('.loading').fadeOut(1000);
    });
    // create wow
    new WOW().init();
});